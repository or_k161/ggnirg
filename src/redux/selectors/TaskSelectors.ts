import {IReducers} from "../../models/Interfaces";
import {createSelector} from "reselect";

const getTasks = (state: IReducers) => state.tasks.tasks

export const getVisibleDrivers = createSelector(
    [getTasks],
    (tasks) => {
        return tasks.filter(task => task.display).map(task => task.location)
    }
)