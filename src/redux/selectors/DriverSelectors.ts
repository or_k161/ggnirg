import {IReducers} from "../../models/Interfaces";
import {createSelector} from "reselect";

const getVisibilityFilter = (state: IReducers) => state.drivers.filterByName
const getDrives = (state: IReducers) => state.drivers.drivers
const getSortByField = (state: IReducers) => state.drivers.sortByField;
const getIsSortOrder = (state: IReducers) => state.drivers.isAsc;

const getVisibleDrivers = createSelector(
    [getVisibilityFilter, getDrives],
    (visibilityFilter, drivers) => {
        return drivers.filter(driver => (driver.name.first +' '+ driver.name.last).indexOf(visibilityFilter) > -1)
    }
)

export const sortDrivers = createSelector(
    [getVisibleDrivers, getSortByField, getIsSortOrder],
    (drivers, sortType, isAsc) => {
        let sortedDrivers;
        switch (sortType) {
            case "NAME":
                sortedDrivers = drivers.sort((prop1, prop2) => (prop1.name.first > prop2.name.first) ? 1 : -1)
                break;
            case 'AGE':
                sortedDrivers = drivers.sort((prop1, prop2) => (prop1.age > prop2.age) ? 1 : -1)
                break;
            case "NONE":
            default:
                return drivers
        }
        if (isAsc)
            return sortedDrivers.slice(0)
        return sortedDrivers.reverse().slice(0)
    }
);