export type DriverTaskTypes =
    {
        type: 'CONNECT_DRIVER_TASK',
        taskId: string,
        driverId: string

    } |
    {
        type: 'DELETE_DRIVER_TASK',
        driverId: string
    }


export interface IDriverTaskReducers {
    driverTasks: {
        driverId: string
        taskId: string
    }[]
}

export const driverTaskReducer = (state: IDriverTaskReducers = {
    driverTasks: []
}, action: DriverTaskTypes): IDriverTaskReducers => {
    switch (action.type) {
        case 'CONNECT_DRIVER_TASK':
            if (action.driverId === '-1')
                return {
                    ...state,
                    driverTasks: state.driverTasks.filter(driverTask => (driverTask.taskId !== action.taskId))
                };
            return {
                ...state,
                driverTasks: state.driverTasks.concat({driverId: action.driverId, taskId: action.taskId})
            }
        case 'DELETE_DRIVER_TASK':
            return {
                ...state,
                driverTasks: state.driverTasks.filter(driverTask => driverTask.driverId !== action.driverId)
            }
        default:
            return state;
    }
}