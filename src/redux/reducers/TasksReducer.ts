import {ITask} from "../../models/Interfaces";

export type taskActionsTypes =
    {
        type: 'FETCH_TASKS',
        tasks: ITask[]

    } |
    {
        type: 'DISPLAY_TASK_EVENT',
        taskId: string

    }


export interface ITaskReducerState {
    tasks: ITask[]
}

export const taskReducer = (state: ITaskReducerState = {tasks: []}, action: taskActionsTypes): ITaskReducerState => {
    switch (action.type) {
        case 'FETCH_TASKS':
            return {
                ...state,
                tasks: action.tasks.map((task) => {
            return {
                ...task, display: true, location: {
                    longitude: parseFloat(task.location.longitude.toString()), latitude: parseFloat
                    (task.location.latitude.toString())
                }
            }
        })
            }
        case 'DISPLAY_TASK_EVENT':
            return {
                ...state,
                tasks: state.tasks.map(task => {
                    return task._id === action.taskId ? {...task, display: !task.display} : task
                })
            }
        default:
            return state;
    }
}