import {IDriver} from "../../models/Interfaces";

export type driversActionsTypes =
    {
        type: "FETCH_DRIVERS",
        drivers: IDriver[]

    } |
    {
        type: 'DELETE_DRIVER',
        driverId: string,
    } |
    {
        type: 'FILTER_BY_NAME',
        filterName: string
    } |
    {
        type: 'SORT_DRIVERS',
        field: string
    } |
    {
        type: 'SORT_DRIVERS_BY_FIELD',
        sortField: string

    }

export interface IDriverReducerState {
    drivers: IDriver[],
    filterByName: string,
    sortByField: string
    isAsc: boolean
}

export const driversReducer = (state: IDriverReducerState = {
    drivers: [],
    filterByName: '', sortByField: "NONE", isAsc: false
}, action: driversActionsTypes): IDriverReducerState => {
    switch (action.type) {
        case 'FETCH_DRIVERS':
            return {
                ...state,
                drivers: action.drivers.map((driver) => {
                    return {
                        ...driver, location: {
                            longitude: parseFloat(driver.location.longitude.toString()), latitude: parseFloat
                            (driver.location.latitude.toString())
                        }
                    }
                })
            };
        case 'DELETE_DRIVER':
            return {
                ...state,
                drivers: state.drivers.filter((driver) => driver._id !== action.driverId)
            };
        case 'FILTER_BY_NAME':
            return {
                ...state,
                filterByName: action.filterName
            };

        case 'SORT_DRIVERS_BY_FIELD':
            return {
                ...state,
                sortByField: action.sortField,
                isAsc: !state.isAsc
            }
        default:
            return state;
    }
}