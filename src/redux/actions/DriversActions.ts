import axios from "axios";

export enum DriversActionsEnum {
    FETCH_DRIVERS,
    DELETE_DRIVER
}

export const fetchDriversAction = () => {
    return async (dispatch) => {
        let driversData;
        try {
            const drivers=await axios.get('https://next.json-generator.com/api/json/get/Ny8G4KT7H')
            driversData=drivers.data
        }
        catch (e) {
            driversData=[]
        }
            dispatch( {
                type: 'FETCH_DRIVERS',
                drivers: driversData
            })

    }
};

export const deleteDriverAction = (driverId: string) => {
    return {
        type: 'DELETE_DRIVER',
        driverId: driverId
    }
}
export const filterDriverByNameAction = (filterName: string) => {
    return {
        type: 'FILTER_BY_NAME',
        filterName
    }
}
export const sortDriversByFieldAction = (sortField: string) => {
    return {
        type: 'SORT_DRIVERS_BY_FIELD',
        sortField
    }
}


