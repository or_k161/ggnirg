import axios from "axios";


export const fetchTasksAction = () => {
    return async (dispatch) => {
        let tasksData
        try {
            const tasksFromServer=await axios.get('https://next.json-generator.com/api/json/get/Vk8YSY6QB')
            tasksData=tasksFromServer.data
        }
        catch (e) {
            tasksData=[];
        }
        dispatch( {
            type: 'FETCH_TASKS',
            tasks:tasksData
        })
    }
};

export const changeDisplayAction = (taskId:string) => {
    return {
        type: 'DISPLAY_TASK_EVENT',
        taskId:taskId
    }
};

