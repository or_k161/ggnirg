export const connetDriverToTask = (taskId: string, driverId: string) => {
    return {
        type: 'CONNECT_DRIVER_TASK',
        taskId: taskId,
        driverId: driverId
    }
};
export const deleteDriverTask = (driverId: string) => {
    return {
        type: 'DELETE_DRIVER_TASK',
        driverId: driverId
    }
};

