
import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './Components/App';
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, createStore, Reducer} from "redux";
import {IReducers} from "./models/Interfaces";
import {driversReducer} from "./redux/reducers/DriversReducer";
import {taskReducer} from "./redux/reducers/TasksReducer";
import {driverTaskReducer} from "./redux/reducers/DriverTaskReducers";

const reducers: Reducer<IReducers> = combineReducers({
    drivers: driversReducer,
    tasks:taskReducer,
    driverTask:driverTaskReducer
});

const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(<Provider store={store}>
    <App/>
</Provider>, document.getElementById('root'));
