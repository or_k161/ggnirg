import {IDriverReducerState} from "../redux/reducers/DriversReducer";
import {ITaskReducerState} from "../redux/reducers/TasksReducer";
import {IDriverTaskReducers} from "../redux/reducers/DriverTaskReducers";

export interface IDriver {
    _id: string,
    name: {first:string,last :string}
    age: number,
    location: {
        latitude: number ,
        longitude: number
    },
    picture: string
    phone: string
}

export interface ITask {
    _id: string,
    title: string,
    scheduled_at: string,
    location: {
        latitude: number,
        longitude: number
    },
    display?: boolean
    address: string
}


export interface IReducers {
    drivers: IDriverReducerState
    tasks: ITaskReducerState
    driverTask: IDriverTaskReducers
}

export interface ILocation {
    latitude: number ,
    longitude: number
}