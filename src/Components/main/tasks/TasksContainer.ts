import {connect} from "react-redux";
import {IReducers} from "../../../models/Interfaces";
import {Tasks} from "./Tasks";
import {changeDisplayAction, fetchTasksAction} from "../../../redux/actions/TasksActions";
import {connetDriverToTask} from "../../../redux/actions/DriverTaskActions";

const mapStateToProps = (state: IReducers) => {
    return {
        tasks: state.tasks.tasks,
        drivers: state.drivers.drivers
    }
};

const matchDispatchToProps = (dispatch) => {
    return {
        fetchTasks: () => dispatch(fetchTasksAction()),
        assignTask: (taskId: string, driverId: string) => dispatch(connetDriverToTask(taskId, driverId)),
        changeDisplay: (taskId: string) => dispatch(changeDisplayAction(taskId))
    }
};


export default connect(mapStateToProps, matchDispatchToProps)(Tasks)