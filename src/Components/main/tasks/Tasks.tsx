import * as React from 'react';
import './Tasks.css'
import {IDriver, ITask} from "../../../models/Interfaces";
import {Task} from "./task/Task";

interface TasksProps {
    tasks: ITask[]
    drivers: IDriver[]
    fetchTasks: () => void
    assignTask: (taskId: string, driverId: string) => void
    changeDisplay: (taskId: string) => void
}

export class Tasks extends React.Component <TasksProps, {}> {
    constructor(props: TasksProps) {
        super(props)
    }

    componentDidMount() {
        this.props.fetchTasks()
    }

    render() {
        return (
            <table className='fixed_header'>
                <thead>
                <tr>
                    <th className='task-table-title'>Title</th>
                    <th className='task-table-scheduled'>Scheduled for</th>
                    <th className='task-table-assign'>Assign to</th>
                    <th className='task-table-address'>Address</th>
                    <th className='task-table-lat'>LAT</th>
                    <th className='task-table-lng'>LNG</th>
                    <th className='task-table-display'>Display</th>
                </tr>
                </thead>
                <tbody>
                {this.props.tasks.map(task => <Task changeDisplay={this.props.changeDisplay} key={task._id}
                                                    assignTask={this.props.assignTask} task={task}
                                                    drivers={this.props.drivers}/>)}
                </tbody>
            </table>
        );
    }
}



