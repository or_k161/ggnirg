import {FunctionComponent} from "react";
import * as React from "react";
import {IDriver, ITask} from "../../../../models/Interfaces";
import './Task.css'

interface ITaskProps {
    task: ITask
    drivers: IDriver[]
    assignTask: (taskId: string, driverId: string) => void
    changeDisplay: (taskId: string) => void

}

export const Task: FunctionComponent<ITaskProps> = (props) => {
    const {drivers, task} = props

    const handleSelectDriver = (event: React.ChangeEvent<HTMLSelectElement>) => {
        props.assignTask(props.task._id, event.target.value)
    };

    const handleDisplayCheckbox = () => {
        props.changeDisplay(task._id)
    };

    return (<tr key={task._id}>
        <td className='task-table-title'>{task.title}</td>
        <td className='task-table-scheduled'>{task.scheduled_at}</td>
        <td className='task-table-assign'>
            <select className='select-drivers' onChange={handleSelectDriver}>
                <option value={-1}>Select Driver</option>
                {drivers.map(driver => {
                    return <option key={driver._id}
                                   value={driver._id}>{driver.name.first + ' ' + driver.name.last}</option>
                })}
            </select>
        </td>
        <td className='task-table-address'>{task.address}</td>
        <td className='task-table-lat'>{task.location.latitude}</td>
        <td className='task-table-lng'>{task.location.longitude}</td>
        <td className='task-table-display'>
            <div className="checkboxFive">
                <input onChange={handleDisplayCheckbox} type="checkbox" checked={task.display} id="checkboxFiveInput"
                       name=""/>
                <label htmlFor="checkboxFiveInput"></label>
            </div>
        </td>
    </tr>)


}
