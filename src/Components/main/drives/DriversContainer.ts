import {connect} from "react-redux";
import {Drivers} from "./Drivers";
import {
    deleteDriverAction,
    fetchDriversAction,
    filterDriverByNameAction,
    sortDriversByFieldAction
} from "../../../redux/actions/DriversActions";
import {deleteDriverTask} from "../../../redux/actions/DriverTaskActions";
import {sortDrivers} from "../../../redux/selectors/DriverSelectors";
import {getVisibleDrivers} from "../../../redux/selectors/TaskSelectors";


const mapStateToProps = (state) => {
    return {
        drivers: sortDrivers(state),
        driverTasks: state.driverTask.driverTasks,
        tasksLocation: getVisibleDrivers(state)
    }
};

const matchDispatchToProps = (dispatch) => {
    return {
        fetchDrivers: () => dispatch(fetchDriversAction()),
        deleteDriverById: (driverId: string) => {
            dispatch(deleteDriverAction(driverId))
            dispatch(deleteDriverTask(driverId))
        },
        filterDriverByName: (filterName: string) => dispatch(filterDriverByNameAction(filterName)),
        sortDriversByField: (fieldSort: string) => dispatch(sortDriversByFieldAction(fieldSort)),
    }
};


export default connect(mapStateToProps, matchDispatchToProps)(Drivers)
