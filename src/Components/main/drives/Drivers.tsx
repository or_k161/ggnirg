import * as React from 'react';
import './Drivers.css'
import {Driver} from "./driver/Driver";
import {IDriver, ILocation} from "../../../models/Interfaces";
import {DriversMap} from "./map/Map";

interface DriversProps {
    drivers: IDriver[]
    tasksLocation: ILocation[]
    deleteDriverById: (driverId: string) => void
    filterDriverByName: (filterName: string) => void
    sortDriversByField: (fieldToSort: string) => void
    fetchDrivers: () => void
    driverTasks: {
        driverId: string
        taskId: string
    }[]
}

interface IDriversState {
    center: { lat: number, lng: number },
    nameSort: { isSorted: boolean, isAsc: boolean }
    ageSort: { isSorted: boolean, isAsc: boolean }
}


export class Drivers extends React.Component <DriversProps, IDriversState> {

    constructor(props: DriversProps) {
        super(props)
        this.state = {
            center: {lat: 0, lng: 0},
            nameSort: {isAsc: true, isSorted: false},
            ageSort: {isAsc: true, isSorted: false}
        }
    }

    componentDidMount() {
        this.props.fetchDrivers()
    }

    render() {
        return (
            <div className='drivers'>
                <span className="drivers-border">
                    <input className="filter-input-field" onChange={this.handleFilterDriversByName}
                           placeholder='Filter Name ...'/>
                    <div className='drivers-list'>
                        <div className='drivers-list-header'>
                            <span>Drivers List</span>
                            <span className='drivers-sort-buttons'>
                                <button onClick={() => this.props.sortDriversByField("NAME")}>Name Sort </button>
                                <button onClick={() => this.props.sortDriversByField("AGE")}>  Age Sort  </button>
                                </span>
                        </div>
                        <div className='driver-list-container'>
                            {this.getDrivers()}
                        </div>
                    </div>
                </span>
                <span className='driver-map'>
                    <DriversMap tasksLocation={this.props.tasksLocation}
                                driversLocation={this.props.drivers.map(driver => driver.location)} center={this.state.center}/>
                </span>
            </div>
        );
    }

    getDrivers = () => {
        return this.props.drivers.map(driver => {
            return <Driver key={driver._id} name={driver.name} _id={driver._id} age={driver.age}
                           tasksLength={this.props.driverTasks.filter(driverTask => driverTask.driverId === driver._id).length}
                           picture={driver.picture}
                           deleteDriverById={this.props.deleteDriverById}
                           toLocate={() => this.handleLocationDriver(driver.location.latitude, driver.location.longitude)}/>
        })
    }
    handleLocationDriver = (lat: number, lng: number) => {
        this.setState({center: {lat: lat, lng: lng}})
    }

    handleFilterDriversByName = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.props.filterDriverByName(event.target.value)
    }

}
