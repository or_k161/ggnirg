import * as React from 'react'
import GoogleMapReact from "google-map-react";
import './TasksMap.css'
import {ILocation} from "../../../../models/Interfaces";

interface MapProps {
    center: {
        lat: number,
        lng: number
    }
    driversLocation: ILocation[]
    tasksLocation: ILocation[]
}

const DrawOnMap = ({lat, lng, text}) => <div>{text}</div>;

export class DriversMap extends React.Component <MapProps, {}> {

    render() {
        return (
            <div className='map-border'>
                <GoogleMapReact
                    center={{lat: this.props.center.lat, lng: this.props.center.lng}}
                     bootstrapURLKeys={{ key:'AIzaSyDIsZLBhSEhgjz2OWxOHJ-TRo3k5RtEaq0'}}
                    defaultZoom={13}
                >
                    {this.getDriversLocationOnMap()}
                    {this.getTasksLocationOnMap()}
                </GoogleMapReact>
            </div>
        )
    }

    getDriversLocationOnMap = () => {
        return this.props.driversLocation.map(taskLocation => {
            return <DrawOnMap key={taskLocation.latitude} lat={taskLocation.latitude} lng={taskLocation.longitude} text='D'/>
        })
    }
    getTasksLocationOnMap = () => {
        return this.props.tasksLocation.map(taskLocation => {
            return <DrawOnMap key={taskLocation.latitude} lat={taskLocation.latitude} lng={taskLocation.longitude} text='T'/>
        })
    }
}
