import * as React from 'react'
import './Driver.css'

interface DriverProps {
    _id: string
    name: { first: string, last: string }
    age: number
    picture: string
    tasksLength: number
    deleteDriverById: (driverId: string) => void
    toLocate: () => void
}

export class Driver extends React.Component <DriverProps, {}> {

    deleteHandler = () => {
        this.props.deleteDriverById(this.props._id)
    }

    render() {
        const {name, age, tasksLength, picture} = this.props;
        return (
            <div className='driver'>
                <div>
                    <img className='driver-img' src={picture}/>
                    <div className='driver-task'>Task: {tasksLength}</div>
                </div>

                <div className='driver-data'>
                    <div className='driver-name'>{name.first + ' ' + name.last}</div>
                    <div className='driver-age'>Age: {age}</div>
                </div>

                <div className='driver-button'>
                    <button className='driver-location-button' onClick={this.props.toLocate}>location</button>
                    <button className='driver-remove-button' onClick={this.deleteHandler}> remove</button>
                </div>
            </div>


        )
    }

}