import * as React from 'react';
import './Main.css'
import Drivers from "./drives/DriversContainer";
import Tasks from "./tasks/TasksContainer";

export class Main extends React.Component <{}, {}> {

    render() {
        return (
            <div className="main-border">
                <div className='main-padding'>
                    <Drivers/>
                </div>
                <div className='main-footer'>
                    <Tasks/>
                </div>
            </div>
        );
    }
}


