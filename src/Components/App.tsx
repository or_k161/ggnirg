import * as React from 'react';
import './App.css'
import {Main} from "./main/Main";

export class App extends React.Component <{}, {}> {

    render() {
        return (
            <div className="app">
                <h3 className="app-header">Welcome to Drivers App</h3>
                <Main/>
            </div>
        );
    }
}


